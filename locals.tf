
locals {
  vpc_id  = aws_vpc.vamonvpc.id
  az_pub  = data.aws_availability_zones.available.names[1]
  az_pub_2  = data.aws_availability_zones.available.names[2]
  az_priv =data.aws_availability_zones.available.names[0]
}