
# Declare the data source
data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_key_pair" "vamon-key" {
  key_name           = "vamon-key"

}

resource "aws_vpc" "vamonvpc" {
  cidr_block = var.vpc_cider
  enable_dns_support = true
  enable_dns_hostnames = true
  instance_tenancy = "default"

  tags = {
    Name = upper("vamonvpc_${terraform.workspace}")
  }
}

resource "aws_subnet" "private_subnet" {
  vpc_id     = local.vpc_id
  cidr_block = var.private_subnet
  availability_zone = data.aws_availability_zones.available.names[0]
  map_public_ip_on_launch = true 

  tags = {
    Name = upper("private_subnet-${terraform.workspace}")
  }
}

resource "aws_subnet" "public_subnet" {
  vpc_id     = local.vpc_id
   cidr_block = var.public_subnet
   availability_zone = data.aws_availability_zones.available.names[1]
   map_public_ip_on_launch = true 


  tags = {
    Name = upper("public_subnet-${terraform.workspace}")
  }
}


resource "aws_subnet" "public_subnet_2" {
  vpc_id     = local.vpc_id
   cidr_block = var.public_subnet_2
   availability_zone = data.aws_availability_zones.available.names[2]
   map_public_ip_on_launch = true 


  tags = {
    Name = upper("public_subnet_2-${terraform.workspace}")
  }
}

resource "aws_instance" "webapp" {
  ami           = "ami-090fa75af13c156b4"
  instance_type = "t2.micro"
  key_name = data.aws_key_pair.vamon-key.key_name
  #subnet_id = "subnet-03c2620fc973a2efe"
  tags = {
    Name =  upper("webappinstanc-${terraform.workspace}")
  }
}