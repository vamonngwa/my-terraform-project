
terraform { 

    backend "s3" {
    bucket = "vamonstatebucket"
    dynamodb_table = "terraform-state-lock"
    region = "us-east-1"
    key = "path/env"
    encrypt = true
}
  
}