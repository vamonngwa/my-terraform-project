
#create a variable
variable "vpc_cider" {
description = "vpc_cider"
type = string 
default = "10.0.0.0/16"
}

variable "vpc_tag" {
description = "vpc_tag"
type = string
default = "vamonvpc"
}

variable "private_subnet" {
description = "private subnet"
type = string
default = "10.0.1.0/24"
}

variable "private_subnet_az" {
description = "private subnet_az"
type = string
default = "us-east-1a"
}

variable "public_subnet" {
description = "public  subnet"
type = string
default = "10.0.0.0/24"
}

variable "public_subnet_az" {
description = "public  subnet"
type = string
default = "us-east-1b"
}

variable "public_subnet_2" {
description = "public  subnet"
type = string
default = "10.0.2.0/24"
}

variable "public_subnet_2_az" {
description = "public  subnet"
type = string
default = "us-east-1c"
}
